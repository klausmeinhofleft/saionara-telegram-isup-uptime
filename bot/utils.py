from functools import wraps
import re

from telegram.ext import ConversationHandler

from bot.db import Admin

def restricted(func):
    @wraps(func)
    def wrapped(update, context, *args, **kwargs):
        username = update.effective_user.username
        # https://docs.peewee-orm.com/en/latest/peewee/quickstart.html#lists-of-records
        admins = [admin.username for admin in Admin.select()]
        if username not in admins:
            print("Unauthorized access denied for {}.".format(username))
            #update.message.reply_text('🔐 Solo les admins pueden hacer esto!')
            update.message.reply_text('🔐 Este bot solo lo puede usar gente autorizada!')
            return
        return func(update, context, *args, **kwargs)
    return wrapped

def secs_to_text(secs):
    # si en más de media hora lo mostramos en horas
    if secs > 60 * 60 / 2:
        horas = round(secs/(60*60), 1)
        horas_round = round(horas)
        return str(horas_round if horas == horas_round else horas) + ' horas'
    else:
        return f'{secs} segundos'

def escape_markdown_entities(text):
    return text.replace('.', '\.').replace('(', '\(').replace(')', '\)')

def remove_markdown_entities(markdown):
    # no me funcionaba en non-capture group (?:) así que lo partí en dos
    sub1 = re.sub(r'(__|[*~_`])([^ ])', r'\2', markdown)
    sub2 = re.sub(r'([^ ])(__|[*~_`])', r'\1', sub1)
    return sub2
