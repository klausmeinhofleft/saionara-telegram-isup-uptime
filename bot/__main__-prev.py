import logging, sys
from pprint import pprint

# github - https://github.com/python-telegram-bot/python-telegram-bot
# code snippets - https://github.com/python-telegram-bot/python-telegram-bot/wiki/Code-snippets
# reference - https://python-telegram-bot.readthedocs.io/
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler
from dotenv import load_dotenv
load_dotenv(verbose=True)

from bot.env import try_get_multiple
#from bot.db import db, recreate
#from bot.job import add_msg_job
#import bot.commands as commands

BOT_TOKEN, LOGGING, BOTS_CHANNEL, UPTIME_OK_STR = try_get_multiple('BOT_TOKEN, LOGGING, BOTS_CHANNEL, UPTIME_OK_STR')
LOGGING = LOGGING.lower() in ['1', 'true', 'yes', 'y']

# creamos la DB si no tiene data
#if not db.get_tables():
#    recreate(force=True)

# Enable logging
if LOGGING:
    import traceback
    # a mejorar siguiendo: https://github.com/python-telegram-bot/python-telegram-bot/wiki/Code-snippets#an-good-error-handler
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(name)s - %(message)s', level=logging.INFO)
    logger = logging.getLogger(__name__)
    def error(update, context):
        """Log Errors caused by Updates."""
        trace = "".join(traceback.format_tb(sys.exc_info()[2]))
        logger.error('Update %s caused error: %s. Traceback:\n%s', update.update_id if update else '-', context.error, trace)
        logger.error(context.error)
        try: update.message.reply_text(f'📛 Ups, me crashié!')
        except Exception: pass

'''def poll_wiki_thread(context):
    print('poll wiki messages...')
    if not queue.empty():
        print('new wiki messages!')
        msgs = []
        while 1:
            msg = queue.get()
            msgs.append(msg)
            if queue.empty():
                break
        msgs_str = '\n'.join(msgs)
        #context.bot.send_message(chat_id=msg.grupo, text=f'Errores detectodos:\n{msgs_str}', parse_mode='MarkdownV2')
        #context.bot.send_message(chat_id='https://t.me/saio_nara', text=f'Errores detectodos:\n{msgs_str}')
        #context.bot.send_message(chat_id='saio_nara', text=f'Errores detectodos:\n{msgs_str}')
        context.bot.send_message(chat_id='@grupo_de_testeo', text=f'Errores detectodos:\n{msgs_str}')'''

poll_job_name = 'poll_job_name'
poll_job_interval = 4

def uptime_ok_received(update, context):
    msg_text = update.channel_post.text if update.channel_post else None
    print('msg new group', msg_text)

    import datetime
    now = datetime.datetime.now()
    #2021-04-11T22:57:57.328701
    now_str = now.isoformat()[:10+1+8]

    context.bot.send_message(chat_id=BOTS_CHANNEL, text=f'[{now_str}] uptime-is-alive ok!')
    context.bot_data['uptime-is-alive-wait'] = None

    jobs = context.job_queue.get_jobs_by_name(poll_job_name)
    for j in jobs: j.schedule_removal()
    context.job_queue.run_repeating(check_uptime, interval=poll_job_interval, name=poll_job_name)
    print('check_uptime thread reiniciado')


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    #https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.updater.html
    updater = Updater(BOT_TOKEN, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    #bot_username = dp.bot.name
    #breakpoint()

    if LOGGING:
        # log all errors
        dp.add_error_handler(error)

    # on different commands - answer in Telegram
    #dp.add_handler(CommandHandler("help", help))
    #for command_handler in commands.all_command_handlers:
    #    dp.add_handler(command_handler)

    # Filters -https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.filters.html#telegram.ext.filters.Filters
    # Filters.chat - https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.filters.html#telegram.ext.filters.Filters.chat
    #dp.add_handler(MessageHandler(Filters.chat('@grupo_de_testeo'), callback_method))
    # este es el que va
    #dp.add_handler(MessageHandler(Filters.chat(username='@grupo_de_testeo'), callback_method))
    #dp.add_handler(MessageHandler(Filters.chat(username='@testchan6655'), callback_method))
    #dp.add_handler(MessageHandler(Filters.chat(username=BOTS_CHANNEL) & Filters.regex(bot_username) & Filters.regex(UPTIME_OK_STR), callback_method))
    dp.add_handler(MessageHandler(Filters.chat(username=BOTS_CHANNEL) & Filters.regex(UPTIME_OK_STR), uptime_ok_received))
    #dp.add_handler(MessageHandler(Filters.chat(username='@grupo_de_testeo') & Filters., callback_method))
    #dp.add_handler(MessageHandler(Filters.chat_type.group, callback_method))
    #dp.add_handler(MessageHandler(Filters.all, callback_method))
    #dp.add_handler(MessageHandler(Filters.text, callback_method))
    #dp.add_handler(MessageHandler(Filters.text & Filters.chat_type.groups, callback_method))
    print('handler de grupo agregado')

    # Start the Bot
    try:
      updater.start_polling()
    except telegram.error.Unauthorized as e:
      print('Api token inválido. Err:', str(e))
      sys.exit(1)

    # arrancamos el job
    #add_msg_job(updater)

    dp.bot_data['uptime-is-alive-wait'] = None
    # run_repeating - https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.jobqueue.html#telegram.ext.JobQueue.run_repeating
    updater.job_queue.run_repeating(check_uptime, interval=poll_job_interval, name=poll_job_name)
    print('check_uptime thread encolado')
    #print('NOOOOOOOcheck_uptime thread encolado')

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    print('Bot corriendo')
    #https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.updater.html#telegram.ext.Updater.idle
    updater.idle()
    print('Bot cerrado')

'''#https://stackoverflow.com/questions/6920858/interprocess-communication-in-python
#https://stackoverflow.com/a/61771563
from multiprocessing.connection import Listener
def listen():
    listener = Listener(('localhost', 6000), authkey=b'secret password')
    running = True
    while running:
        conn = listener.accept()
        print('connection accepted from', listener.last_accepted)
        while True:
            msg = conn.recv()
            print(msg)
            if msg == 'close connection':
                conn.close()
                break
            if msg == 'close server':
                conn.close()
                running = False
                break
    listener.close()'''

def check_wiki():
    pass
    '''#https://www.geeksforgeeks.org/get-method-python-requests/
    #pip install requests
    import requests

    # Making a GET request
    #r = requests.get('https://api.github.com / users / naveenkrnl')
    r = requests.get('https://wiki.rlab.be')

    # check status code for response received
    # success code - 200
    print(r)

    # print content of request
    print(r.content)
    open('res', 'w+').write(r.content)'''

    '''#https://stackoverflow.com/questions/645312/what-is-the-quickest-way-to-http-get-in-python
    import urllib.request
    #https://docs.python.org/3/library/urllib.request.html
    # url, headers, and status.
    r = urllib.request.urlopen("https://wiki.rlab.be")
    if r.status != 200:
        contents = r.read()
        print(contents[:50])
    else:
        import datetime
        now = datetime.datetime.now()
        #2021-04-11T22:57:57.328701
        now_str = now.isoformat()[:10+1+8]
        queue.put(f'[{now_str}] Wiki down (code {r.status})')'''

#from multiprocessing import Process, Queue
# put get empty
#queue = Queue()

def check_uptime(context):
    print('check_uptime...')

    chat_id = '@testchan6655'

    import datetime
    now = datetime.datetime.now()
    #2021-04-11T22:57:57.328701
    now_str = now.isoformat()[:10+1+8]

    try:
        if context.bot_data['uptime-is-alive-wait']:
            date_last = context.bot_data['uptime-is-alive-wait']
            date_delta = now - date_last
            # 0:00:03.000376 | total_seconds, microseconds, seconds
            #date_delta_str = date_delta.strftime('%M:%S')
            date_delta_str = round(date_delta.total_seconds())
            context.bot.send_message(chat_id=chat_id, text=f'[{now_str}] uptime-is-alive? waiting {date_delta_str}s')
        else:
            context.bot.send_message(chat_id=chat_id, text=f'[{now_str}] uptime-is-alive? now')
            context.bot_data['uptime-is-alive-wait'] = now

    except telegram.error.Unauthorized as e:
        # Forbidden: bot is not a member of the supergroup chat
        print(f'ERROR FATAL: sin acceso a chat {chat_id}')

    #https://stackoverflow.com/a/61771563
    '''from multiprocessing.connection import Client

    chat_id = '@grupo_de_testeo'

    import datetime
    now = datetime.datetime.now()
    #2021-04-11T22:57:57.328701
    now_str = now.isoformat()[:10+1+8]

    try:
        conn = Client(('localhost', 6000), authkey=b'secret password')
        print('check_uptime ok')
        #conn.send('bar')
        #conn.send('close server')
        #https://docs.python.org/3/library/multiprocessing.html#multiprocessing-listeners-clients
        r = conn.recv()
        print(r)
        conn.close()
        try:
            context.bot.send_message(chat_id=chat_id, text=f'[{now_str}] Uptime bot ok')
        except telegram.error.Unauthorized as e:
            # Forbidden: bot is not a member of the supergroup chat
            print(f'ERROR FATAL: sin acceso a chat {chat_id}')
    except ConnectionRefusedError as e:
        print('check_uptime falló...')
        # [Errno 111] Connection refused
        try:
            context.bot.send_message(chat_id=chat_id, text=f'[{now_str}] Uptime bot caído')
        except telegram.error.Unauthorized as e:
            # Forbidden: bot is not a member of the supergroup chat
            print(f'ERROR FATAL: sin acceso a chat {chat_id}')'''

if __name__ == '__main__':

    #https://algorithmtraining.com/inter-process-communication-in-python/
    #proc1 = Process(target=main)
    #proc1.start()
    #print('Proceso main arrancado')

    '''proc2 = Process(target=listen)
    proc2.start()
    print('Proceso listen arrancado')

    proc1.join()
    print('Proceso main terminado')
    proc2.join()
    print('Proceso listen terminado')'''

    main()

    '''#https://github.com/dbader/schedule
    import schedule
    import time

    #def job():
    #    print("I'm working...")

    #schedule.every(10).minutes.do(job)
    schedule.every(3).seconds.do(check_wiki)
    while True:
        schedule.run_pending()
        time.sleep(2)
        print("poll check wiki...")'''

    #print('Proceso main a la espera...')
    #proc1.join()
    #print('Proceso main terminado')
