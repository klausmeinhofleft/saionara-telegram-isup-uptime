from telegram.ext import CommandHandler

from bot.db import get_msg_guita
from bot.job import add_msg_job
from bot.utils import restricted
from .cancel import cancel

def create_handler():
    return CommandHandler("activar", command)

@restricted
def command(update, context):
    msg = get_msg_guita()

    # validaciones
    if msg.activo:
        update.message.reply_text('❇️ Ya estoy activo')
        return

    # updateamos DB
    msg.activo = True
    msg.save()

    # agregamos el job
    add_msg_job(context)

    # devolvemos
    update.message.reply_text('✅ Activado')
