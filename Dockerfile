# no usamos alpine porque trae muchos problemas para buildear
# mirar https://pythonspeed.com/articles/alpine-docker-python/
# set base image (host OS)
# https://hub.docker.com/_/python/
FROM python:3-slim-buster

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# copy the content of the local src directory to the working directory
COPY bot bot

# command to run on container start
# -u para que flushee los mensajes a output/log, sino no se ve nada
CMD [ "python", "-um", "bot" ]
