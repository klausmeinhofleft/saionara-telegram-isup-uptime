#https://stackoverflow.com/questions/6920858/interprocess-communication-in-python
from multiprocessing.connection import Listener
def listen():
    listener = Listener(('localhost', 6000), authkey=b'secret password')
    running = True
    while running:
        conn = listener.accept()
        print('connection accepted from', listener.last_accepted)
        while True:
            msg = conn.recv()
            print(msg)
            if msg == 'close connection':
                conn.close()
                break
            if msg == 'close server':
                conn.close()
                running = False
                break
    listener.close()

if __name__ == '__main__':
    listen()
